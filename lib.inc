%define exit_code 60
section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, exit_code                
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .cycle: 
        cmp byte[rdi + rax], 0; проверяем не конец-ли строки 
        je .break      ; иначе выходим
        inc rax             ; считаем длину строки         
        jmp .cycle          ; повторяем цикл
    .break:
        ret   


; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length          ; вызываем функцию возвращающую длину строки
    pop rdi
    mov rdx, rax                ; передаем значения 
    mov rax, 1                  ; write
    mov rsi, rdi                ; передаем значения строки в rsi
    mov rdi, 1                  ; передаем значение stdout
    syscall
    ret
;
; Принимает код символа и выводит его в stdout
print_char:
    push rdi    ; поместили в стек
    mov rax, 1  ; write
    mov rsi, rsp
    mov rdx, 1 ; передаем длину сивола
    mov rdi, 1 ; передаем значение stdout
    syscall 
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    jmp print_char
    

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    mov r9, 10                  ; записали 10 в регистр r9 для дальнейшего деления
    mov r10, 0                  ; занулили счетчик
    .cycle:
        mov rdx, 0
        div r9                  ; делим
        add rdx, 48             ; добавляем 48, чтобы перевести в ASCII
        push rdx
        inc r10
        cmp rax, 0              
        je .break
        jmp .cycle
    .break:
        cmp r10, 0
        je .end
        pop rdi
        push r10
        call print_char
        pop r10
        dec r10
        jmp .break
    .end:
        ret



; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    cmp rdi, 0                  ; если число положительное, то
    jge .end                    ; переходим на завершение

    push rdi                    ; пушим rdi в стек
    mov rdi, '-'                ; записываем в rdi знак -
    call print_char             ; выводим знак -
    pop rdi                     
    neg rdi                     ; меняем знак у числа
    
    .end:
        jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    mov r9, 1 
    .begin:
        mov r10b, byte[rdi]
        cmp r10b, byte[rsi] 
        jne .break 
        cmp byte[rsi], 0 
        je .end
        inc rdi 
        inc rsi
        jmp .begin
    .break:
        mov r9, 0 
    .end:
        mov rax, r9
        ret
    
; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0
    mov rsi, rsp
    xor rax, rax
    xor rdi, rdi
    mov rdx, 1
    syscall
    pop rax
    ret


; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    xor r9, r9                  ; занулили регистр счетчика
    
    .cycle:
        push rdi
        push rsi
        push r9 
        call read_char          ; читаем символ 
        pop r9
        pop rsi
        pop rdi
        

        cmp rax, ` `           ; проверка
        je .signs
        cmp rax, `\t`           ; проверка
        je .signs
        cmp rax, `\n`           ; проверка
        je .signs
        jmp .note
    .note: 
        cmp rsi, r9            ; проверка на место в буфере
        je .stop   
        cmp rax, 0              ; проверка окончания троки 
        je .end
        mov [rdi+r9], rax       ; запись сивола 
        inc r9                  ; inc счетчик
        jmp .cycle
    .signs: 
        cmp r9, 0
        je .cycle
        jmp .end
    .stop:
        xor rax, rax 
        xor rdx, rdx 
        ret
    .end:
        mov rax, 0               ; запись нуль-терминатор
        mov [rdi+r9], rax       ; запись символа
        mov rax, rdi
        mov rdx, r9
        ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor r11, r11
    mov r9, 10 
    xor r8, r8
    .cycle:
        mov r8b, byte[rdi]
        cmp r8b, '0'             ; проверка что число от 0 до 9
        jl .stop
        cmp r8b, '9'
        jg .stop
        cmp r8b, 0
        je .stop
        sub r8b, 48              ; перевод из ASCII
        mul r9                   ; умножаем
        add rax, r8
        inc r11                  ; увеличили счетчик
        inc rdi                  ; переход по строке
        jmp .cycle
    .stop:
        mov rdx, r11
        ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte[rdi], 0
    je .break
    mov r9b, byte[rdi]
    cmp r9b, '-'
    je .neg
    cmp r9b, '+'
    je .pos
    call parse_uint
    jmp .break
    
    .neg:
        inc rdi
        call parse_uint
        cmp rdx, 0
        je .break
        neg rax
        inc rdx
        jmp .break
    .pos:
        inc rdi
        call parse_uint
        inc rdx
    .break:
        ret


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    push rdi
    push rsi
    push rdx
    call string_length
        pop rdx
        pop rsi
        pop rdi
        cmp rax, rdx
        jg .break
        xor rcx, rcx
    .cycle:
        mov r10b, byte[rdi + rcx]  
        mov byte[rsi + rcx], r10b     
        cmp rcx, rax 
        je .end
        inc rcx
        jmp .cycle
    .break:
        xor rax, rax
        ret
    .end:
        mov rax, rcx
        ret
